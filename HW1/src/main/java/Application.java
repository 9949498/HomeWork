package model;

public class Application {
    public static void main(String[] args) {

        /*
        ��� ��������� ������ Kotik. ��� ������� ���������� ������������
 ����������� � �����������, ��� ������� ����������� ��� ���������� � ��������� � �������
 setKotik(int prettiness, String name, int weight, String meow) ��� ������������� ����������.
        */


        Kotik obj1 = new Kotik(3, "Room", 3,
                "Mayyyy-y-y ");
        obj1.eat(20);
        obj1.liveAnotherDay();
        System.out.println("\n" + "-------------------------------");
        Kotik obj2 = new Kotik();
        obj2.eat(13, "������ ���");
        obj2.setKotik(3, "Tom", 3,
                "Gav-gav-v ");
        System.out.println("\n" + "+++++++++++++++++++++++++++++++");
        compareMeow(obj1, obj2);
        System.out.println("\n" + "*******************************");
        Kotik.showCountKotik();
    }

    private static void compareMeow(Kotik cat1, Kotik cat2) {
        String s1 = cat1.getMeow();
        String s2 = cat2.getMeow();
        System.out.println("���� ����� �������:" + s1);
        System.out.println("������ ����� �������:" + s2);
        System.out.println("������ ������� ���������: " + s1.equals(s2));
    }

}

