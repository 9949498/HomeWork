package model;

import java.lang.Math;


class Kotik {
    private int amniam = 0; //�������� ���������� ���������� ������������ ������� ������� ������
    private int weight;  // ���
    private String name; //��� ������
    private static int count = 0; /* ������� : ����������� ����������, ���������� �� ���������� ��������� ����������� ������ */
    private int prettiness;
    private String meow;

    // �������������� ����������� �� ���������
    Kotik() {
        count++;
        setKotik(0, "SomeK", 4, "Mute");
        //show();
    }

    // ����������� �� ���� ��������
    Kotik(int w) {
        count++;
        show();
    }

    //// ����������� �� ��� ���������
    Kotik(int a, String s) {
        count++;
        name = s;
        show();
    }

    // ����������� �� 4 ���������
    Kotik(int prettiness, String name, int weight, String meow) {
        count++;
        System.out.println("����� ������ �� " + prettiness);
        setName(name);
        setWeight(weight);
        setMeow(meow);
        sayMeow(meow);
        setPrettiness(prettiness);
        show();
    }

    /* ������� ��� ���������� ������ eat(), ���� �� ��� ����� ��������� ������ ���������� ��������
������ �������, � ����������� �� ��� ��������������� ���������� ����������, ������ �������
������� � �������� ���, ������ �� ��������� ����������, �� �������� ������ ���� ����� ����������
����������� �������� ��� � ������� �������.
 */
    //#0

    //����� ����
    public boolean eat(int am) {
        amniam = amniam + am;
        System.out.println("����� ����!");
        return true;
    }

    public boolean eat(int am, String s) {
        amniam = amniam + am;
        System.out.println("����� ����." + " \n" +
                "����-  " + s);
        return true;
    }

    public boolean eat() {
        eat(4, "����");
        return true;
    }

    //#1
    protected boolean play() {
        if (amniam > 0) {
            System.out.println("����� ������");
            amniam--;
            return true;
        } else {
            System.out.println("����� ����� ������!");
            eat();
            return false;
        }
    }

    //#2
    protected boolean sleep() {
        if (amniam > 0) {
            System.out.println("����� ����.");
            amniam--;
            return true;
        } else {
            System.out.println("����� ����� ������!");
            eat();
            return false;
        }
    }

    //#3
    protected boolean chaseMouse() {
        if (amniam > 0) {
            System.out.println("����� ����� �����");
            amniam--;
            return true;
        } else {
            System.out.println("����� ����� ������!");
            eat();
            return false;
        }
    }

    //#4
    protected boolean meow() {
        if (amniam > 0) {
            System.out.println("����� ������� ���");
            amniam--;
            return true;
        } else {
            System.out.println("����� ����� ������!");
            eat();
            return false;
        }
    }

    //#5
    protected void sayName() {
        String s;
        if (name == null) {
            s = "No name";
        } else {
            s = getName();
        }
        amniam--;
        System.out.println("������ ����� " + s);
    }

    protected void sayMeow(String meows) {
        setMeow(meows);
        System.out.println("����� ������� " + meows);
    }

    /*������� � ������ ����� ����� liveAnotherDay(), � ������� ����� ���� �� 24 ��������,
 � ������ �� ������� ����� ��������� ������� ���������� ���� �� ������� ������, ���������� ��
  ��� ���������, � ���� ����� ������ ���������� ������ ����� ������� ���� - ��� ���� ����� ���������.
    */
    protected void liveAnotherDay() {
        final int countOneDay = 24;
        int tempCount = 0;

        for (int i = 0; i <= countOneDay; i++) {

            switch ((int) (1 + Math.random() * 5)) {
                case (1): {
                    play();
                    break;
                }
                case (2): {
                    sleep();
                    break;
                }
                case (3): {
                    chaseMouse();
                    break;
                }
                case (4): {
                    meow();
                    break;
                }
                case (5): {
                    sayName();
                    break;
                }
            }
            System.out.println("#" + i);
        }
        System.out.println("����� ������� �������: " + count);
    }

    protected void setWeight(int weight) {
        this.weight = weight;
    }

    protected int getWeight() {
        return this.weight;
    }

    protected String getName() {
        return name;
    }

    protected void setName(String n) {
        this.name = n;
    }

    protected String getMeow() {
        return meow;
    }

    protected void setMeow(String meows) {
        this.meow = meows;
    }

    public void show() {
        if (name == null) {
            name = "No name";
        }

        System.out.println("��� ������ - " + getName());
        //System.out.println("����� ������ �" + count);
        System.out.println("��� ������ ->" + getWeight());
    }

    public static void showCountKotik() {
        System.out.println("����� ������� " + count + " ����������(-��) ������.");
    }

    public void setPrettiness(int prettiness) {
        this.prettiness = prettiness;
    }

    protected void setKotik(int prettiness, String name, int weight, String meow) {
        System.out.println("����� ������ �� " + prettiness);
        setName(name);
        setWeight(weight);
        setPrettiness(prettiness);
        sayMeow(meow);
        show();
    }


}


/*����������� � ������ Kotik �������������� ������ (5 ��),
����������� ��� ���������, �������� play(), sleep(), chaseMouse() � �.�.
��� �� �������� �������� ���������� ���������� ������������
������� ������� ������, ��� ������ ��� ����� ���� ��� ������,
����� ����� ������������ ��������� ����� ��������, ����� eat()

 */